FROM python:alpine

WORKDIR /app

COPY requirements.txt .
COPY . .

RUN pip install --no-cache-dir -r requirements.txt
ENV PYTHONPATH="${PYTHONPATH}:/app/now_playing"
ENTRYPOINT ["python", "now_playing/main.py"]