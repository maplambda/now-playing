import unittest
import urllib, urllib3
import now_playing.oauth2 as oauth2
from tests.helpers import CONFIG, mock_pool


class TestOAuth2(unittest.TestCase):
    def setUp(self):
        self.config = CONFIG
        self.pool = mock_pool()

    def test_authorization_url(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        authorization_url, state = idp.authorization_url()

        self.assertEqual(
            f"https://accounts.spotify.com/authorize?client_id=5fe01282e44241328a84e7c5cc169165&response_type=code&redirect_uri={urllib.parse.quote_plus(self.config.redirect_uri)}",
            authorization_url,
        )
        self.assertEqual(None, state)

    def test_authorization_url_with_state(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        authorization_url, state = idp.authorization_url(state_param="123")
        self.assertEqual(
            f"https://accounts.spotify.com/authorize?client_id=5fe01282e44241328a84e7c5cc169165&response_type=code&redirect_uri={urllib.parse.quote_plus(self.config.redirect_uri)}&state=123",
            authorization_url,
        )
        self.assertEqual("123", state)

    def test_authorization_url_with_scopes(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        authorization_url, state = idp.authorization_url(
            scopes=["user-read-private", "user-read-email"]
        )

        self.assertEqual(
            f"https://accounts.spotify.com/authorize?client_id=5fe01282e44241328a84e7c5cc169165&response_type=code&redirect_uri={urllib.parse.quote_plus(self.config.redirect_uri)}&scope=user-read-private+user-read-email",
            authorization_url,
        )

    def test_parse_callback_uri(self):
        callback_uri = (
            "https://example.com/callback?code=NApCCg..BkWtQ&state=profile%2Factivity"
        )
        code, state = oauth2.AuthorizationCodeFlow.parse_callback_uri(callback_uri)

        self.assertEqual("NApCCg..BkWtQ", code)
        self.assertEqual("profile/activity", state)

    def test_authorization_code_exchange(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        token = idp.authorization_code_exchange(self.pool, "code")

        self.assertEqual("NgCXRK...MzYjw", token.access_token)

    def test_token_refresh(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        token = idp.access_token_refresh(self.pool, "refresh_token")

        self.assertEqual("NgCXRK...MzYjw", token.access_token)
