import unittest
from now_playing.player import Track, TrackBuffer


class TestTrackBuffer(unittest.TestCase):
    def test_empty(self):
        buffer = TrackBuffer()
        self.assertEqual(None, buffer.get())

    def test_buffer(self):
        track_buffer = TrackBuffer(Track("test", 31_000))
        self.assertEqual("test", track_buffer.get().title)

    def test_buffer_append_ready(self):
        track_buffer = TrackBuffer().append(Track("test", 31_000))
        self.assertEqual("test", track_buffer.get().title)

        track_buffer = (
            TrackBuffer().append(Track("test", 1)).append(Track("test", 31_000))
        )
        self.assertEqual("test", track_buffer.get().title)

        track_buffer = (
            TrackBuffer()
            .append(Track("test", 1))
            .append(Track("test", 31_000))
            .append(Track("test", 32_000))
        )
        self.assertIsNone(track_buffer.get())

        track_buffer = (
            TrackBuffer()
            .append(Track("test", 1))
            .append(Track("test", 31_000))
            .append(Track("test2", 32_000))
        )
        self.assertIsNotNone(track_buffer.get())

    def test_buffer_append_not_ready(self):
        track_buffer = TrackBuffer().append(Track("test", 30))
        self.assertIsNone(track_buffer.get())
