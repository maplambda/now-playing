import unittest
import urllib3
from cachetools import TTLCache
import now_playing.oauth2 as oauth2
import now_playing.spotify as spotify
from tests.helpers import CONFIG, mock_pool


class TestSpotify(unittest.TestCase):
    def setUp(self):
        self.config = CONFIG
        self.pool = mock_pool()

    def test_now_paying(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        sp = spotify.Connection(self.pool, idp, "refresh_token")
        player = sp.player()

        args, kwargs = self.pool.urlopen.call_args
        method, request_url = args
        self.assertEqual("https://api.spotify.com/v1/me/player", request_url)
        self.assertEqual("Bearer NgCXRK...MzYjw", kwargs["headers"]["Authorization"])
        self.assertTrue(player["is_playing"])

    def test_auth_request(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        token = spotify.SpotifyToken(self.pool, idp, "refresh_token", TTLCache(1, 3600))
        auth_pool = spotify.OAuth2ConnectionPool(self.pool, token)
        auth_pool.request("GET", "https://api.spotify.com/v1/me/player")

        args, kwargs = auth_pool.pool.urlopen.call_args
        method, request_url = args
        self.assertEqual("https://api.spotify.com/v1/me/player", request_url)
        self.assertEqual("Bearer NgCXRK...MzYjw", kwargs["headers"]["Authorization"])

    def test_spotify_token(self):
        idp = oauth2.AuthorizationCodeFlow(self.config)
        token = spotify.SpotifyToken(self.pool, idp, "refresh_token", TTLCache(1, 3600))
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual(
            1, len(self.pool.urlopen.call_args_list), "connection pool is called once",
        )

        self.pool.urlopen.reset_mock()

        token = spotify.SpotifyToken(self.pool, idp, "refresh_token", TTLCache(1, 0))
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual("NgCXRK...MzYjw", token.get_or_refresh().access_token)
        self.assertEqual(
            3,
            len(self.pool.urlopen.call_args_list),
            "connection pool is called four times",
        )
