import urllib3
from unittest.mock import create_autospec
import now_playing.oauth2 as oauth2

# sample config values from spotify guide
CONFIG = oauth2.OAuth2Config(
    client_id="5fe01282e44241328a84e7c5cc169165",
    client_secret="client_secret",
    access_token_endpoint="https://accounts.spotify.com/api/token",
    authorize_endpoint="https://accounts.spotify.com/authorize",
    redirect_uri="http://localhost",
)


def mock_pool():
    """provides mock response for spotify calls based on oauth2 config"""
    pool = urllib3.PoolManager()
    pool.urlopen = create_autospec(pool.urlopen)

    def url_open_response(method, url, body=None, headers=None, **kwargs):
        response = create_autospec(urllib3.HTTPResponse)
        if method == "POST":
            if url == CONFIG.access_token_endpoint:
                response.data = """
{
  "access_token": "NgCXRK...MzYjw",
  "token_type": "Bearer",
  "scope": "user-read-private user-read-email",
  "expires_in": 3600,
  "refresh_token": "NgAagA...Um_SHo"
}
"""
        else:
            if url == "https://api.spotify.com/v1/me/player":
                response.data = """
{
  "timestamp": 1490252122574,
  "device": {
    "id": "3f228e06c8562e2f439e22932da6c3231715ed53",
    "is_active": false,
    "is_restricted": false,
    "name": "Xperia Z5 Compact",
    "type": "Smartphone",
    "volume_percent": 54
  },
  "progress_ms": "44272",
  "is_playing": true,
  "currently_playing_type": "track",
  "actions": {
    "disallows": {
      "resuming": true
    }
  },
  "item": {},
  "shuffle_state": false,
  "repeat_state": "off",
  "context": {
    "external_urls" : {
      "spotify" : "http://open.spotify.com/user/spotify/playlist/49znshcYJROspEqBoHg3Sv"
    },
    "href" : "https://api.spotify.com/v1/users/spotify/playlists/49znshcYJROspEqBoHg3Sv",
    "type" : "playlist",
    "uri" : "spotify:user:spotify:playlist:49znshcYJROspEqBoHg3Sv"
  }
}                
"""
        return response

    pool.urlopen.side_effect = url_open_response
    return pool
