import urllib3
import json
import logging
from cachetools import TTLCache
from now_playing import oauth2 as oauth2


class SpotifyToken(object):
    def __init__(self, pool, provider, refresh_token, store=TTLCache(1, 3600)):
        self.pool = pool
        self.provider = provider
        self.token_store = store
        self.refresh_token = refresh_token

    def get_or_refresh(self):
        """
        return cached AuthenticationToken or request new token using
        identity provider and connection
        """

        token = self.token_store.get(self.refresh_token)
        if token is None:
            logging.info("ASKING FOR REFRESH")
            new_token = self.provider.access_token_refresh(
                self.pool, self.refresh_token
            )
            self.token_store[self.refresh_token] = new_token
            return new_token
        else:
            return token


class OAuth2ConnectionPool(urllib3.request.RequestMethods):
    def __init__(self, pool, spotify_token):
        self.pool = pool
        self.spotify_token = spotify_token
        super().__init__()

    def urlopen(self, method, url, body=None, headers=None, **kwargs):
        headers["Authorization"] = (
            "Bearer " + self.spotify_token.get_or_refresh().access_token
        )
        return self.pool.urlopen(method, url, body=body, headers=headers, **kwargs)


class Connection(object):
    """
    Provides a connection to spotify API endpoints
    """

    def __init__(self, pool, provider, refresh_token):
        self.auth_pool = OAuth2ConnectionPool(
            pool, SpotifyToken(pool, provider, refresh_token)
        )

    def player(self):
        """
        Get Information About The User's Current Playback
        """

        data = self.auth_pool.request(
            "GET", "https://api.spotify.com/v1/me/player"
        ).data
        return json.loads(data) if data else {}
