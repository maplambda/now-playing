import time


class TrackBuffer(object):
    def __init__(self, track=None, ready=None):
        self.track = track
        self.ready = (
            track.current_position > 30_000 if track and ready is None else ready
        )

    def append(self, track):
        current = self.track
        if current:
            return (
                TrackBuffer(track, track.current_position > 30_000)
                if current.title != track.title
                else TrackBuffer(
                    track,
                    False
                    if self.track.current_position > 30_000
                    else track.current_position > 30_000,
                )
            )
        else:
            return TrackBuffer(track, track.current_position > 30_000)

    def get(self):
        return self.track if self.ready else None


class Track(object):
    def __init__(self, title, current_position):
        self.title = title
        self.current_position = current_position


def tail(spotify_connection):
    track_buffer = TrackBuffer()
    while True:
        data = spotify_connection.player()
        if (
            len(data)
            and data["is_playing"]
            and data["currently_playing_type"] == "track"
        ):
            track_buffer = track_buffer.append(
                Track(
                    "".join(
                        map(lambda x: "(" + x["name"] + ")", data["item"]["artists"])
                    )
                    + " "
                    + data["item"]["name"],
                    data["progress_ms"],
                )
            )
            ready = track_buffer.get()
            if ready:
                yield ready

        time.sleep(10)
