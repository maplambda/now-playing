import logging
import argparse
import os

import urllib3
from pathlib import Path
from pydantic.dataclasses import dataclass
from dotenv import load_dotenv

from now_playing import oauth2 as oauth2
from now_playing import spotify as spotify
from now_playing import player as player


@dataclass
class Settings:
    client_id: str
    client_secret: str
    access_token_endpoint: str
    authorize_endpoint: str
    redirect_uri: str
    refresh_token: str


pool = urllib3.PoolManager()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Logs the currently playing Spotify track to stdout."
    )
    parser.add_argument(
        "--authorize",
        help="exchange AccessToken from authorization uri",
        action="store_true",
    )
    parser.add_argument("--dotenv", help="use .env file", action="store_true")
    args = parser.parse_args()

    if args.dotenv:
        load_dotenv()

    settings = Settings(
        client_id=os.getenv("client_id"),
        client_secret=os.getenv("client_secret"),
        access_token_endpoint=os.getenv("access_token_endpoint"),
        authorize_endpoint=os.getenv("authorize_endpoint"),
        redirect_uri=os.getenv("redirect_uri"),
        refresh_token=os.getenv("refresh_token"),
    )

    idp_config = oauth2.OAuth2Config(
        client_id=settings.client_id,
        client_secret=settings.client_secret,
        access_token_endpoint=settings.access_token_endpoint,
        authorize_endpoint=settings.authorize_endpoint,
        redirect_uri=settings.redirect_uri,
    )

    if args.authorize:
        import webbrowser
        import http.server
        import secrets

        secret = secrets.token_urlsafe()
        idp = oauth2.AuthorizationCodeFlow(idp_config)
        auth_url, state = idp.authorization_url(secret, ["user-read-playback-state"])

        print(auth_url)

        req_complete = False

        def run_server(server_class, handler_class):
            global req_complete
            server_address = ("", 80)
            httpd = server_class(server_address, handler_class)

            while not req_complete:
                httpd.handle_request()

        class AuthorizeRequestHandler(http.server.BaseHTTPRequestHandler):
            def do_GET(self):
                global req_complete
                code, state = oauth2.AuthorizationCodeFlow.parse_callback_uri(self.path)
                assert state == secret

                env_token = f"REFRESH_TOKEN={idp.authorization_code_exchange(pool, code).refresh_token}"
                print(env_token)
                self.wfile.write(env_token.encode())
                req_complete = True

        run_server(http.server.HTTPServer, AuthorizeRequestHandler)

    else:
        idp = oauth2.AuthorizationCodeFlow(idp_config)
        spotify_connection = spotify.Connection(pool, idp, settings.refresh_token)
        try:
            for track in player.tail(spotify_connection):
                print(track.title)

        except KeyboardInterrupt:
            pass
