import urllib, urllib3
import json
import base64
from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass
class OAuth2Config:
    client_id: str
    client_secret: str
    access_token_endpoint: str
    authorize_endpoint: str
    redirect_uri: str


@dataclass
class AuthenticationToken:
    access_token: str
    token_type: str
    scope: str
    expires_in: str
    refresh_token: str


class BaseProvider(ABC):
    def __init__(self, oAuth2Config):
        self.oAuth2Config = oAuth2Config

    @abstractmethod
    def access_token_refresh(self):
        """
        return a new AuthenticationToken
        """
        pass

    @abstractmethod
    def authorization_url(self):
        """
        prepare authorization_url to begin the flow
        """
        pass

    @abstractmethod
    def authorization_code_exchange(self):
        """
        exchange an authorization code for AuthenticationToken
        """
        pass

    @abstractmethod
    def parse_callback_uri(self):
        """
        provide the code and state values from callback uri
        """
        pass


# authoriztion code flow implementation


class AuthorizationCodeFlow(BaseProvider):
    """
    Authorization Code Flow Provider Operations
    """

    def __init__(self, oAuth2Config):
        self.retry = urllib3.util.retry.Retry(5, method_whitelist=False)
        super().__init__(oAuth2Config)

    def authorization_url(self, state_param=None, scopes=[]):
        url_base = self.oAuth2Config.authorize_endpoint
        url_query = {
            "client_id": self.oAuth2Config.client_id,
            "response_type": "code",
            "redirect_uri": self.oAuth2Config.redirect_uri,
        }

        if state_param:
            url_query["state"] = state_param

        if len(scopes):
            url_query["scope"] = " ".join(scopes)

        return ("%s?%s" % (url_base, urllib.parse.urlencode(url_query)), state_param)

    def access_token_refresh(self, connection, refresh_token):
        basic_auth = base64.b64encode(
            "{}:{}".format(
                self.oAuth2Config.client_id, self.oAuth2Config.client_secret
            ).encode("utf-8")
        )
        response = connection.request(
            "POST",
            self.oAuth2Config.access_token_endpoint,
            fields={"grant_type": "refresh_token", "refresh_token": refresh_token},
            headers={"Authorization": "Basic " + basic_auth.decode("utf-8")},
            encode_multipart=False,
            retries=self.retry,
        )
        data = json.loads(response.data)
        return AuthenticationToken(
            data["access_token"],
            data["token_type"],
            data["scope"],
            data["expires_in"],
            None,
        )

    def authorization_code_exchange(self, connection, code):
        basic_auth = base64.b64encode(
            "{}:{}".format(
                self.oAuth2Config.client_id, self.oAuth2Config.client_secret
            ).encode("utf-8")
        )
        response = connection.request(
            "POST",
            self.oAuth2Config.access_token_endpoint,
            fields={
                "grant_type": "authorization_code",
                "redirect_uri": self.oAuth2Config.redirect_uri,
                "code": code,
            },
            headers={"Authorization": "Basic " + basic_auth.decode("utf-8")},
            encode_multipart=False,
            retries=self.retry,
        )
        data = json.loads(response.data)
        return AuthenticationToken(
            data["access_token"],
            data["token_type"],
            data["scope"],
            data["expires_in"],
            data["refresh_token"],
        )

    @staticmethod
    def parse_callback_uri(callback_uri):
        query = urllib.parse.parse_qs(urllib3.util.parse_url(callback_uri).query)
        (code,) = query["code"]
        (state,) = query["state"]
        return (code, state)
