Logs the currently playing Spotify track to stdout if 30 seconds playtime has elapsed. Distinct tracks are only printed once.

![console](carbon.png)

# Usage (docker)
## Configure
1. [Register a Spotify app](https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app)
2. Configure .env 
    ```
    cat << EOF > .env
    >client_id=<client_id>
    >client_secret=<client_secret>
    >access_token_endpoint=https://accounts.spotify.com/api/token
    >authorize_endpoint=https://accounts.spotify.com/authorize"
    >redirect_uri=http://localhost
    >EOF
    ```
3. Build image
``` docker build -t now-playing .```
4. Obtain a refresh token
``` docker run --rm -t -p 80:80 --env-file .env now-playing --authorize```
5. Set refresh token in .env
``` echo REFRESH_TOKEN=....  >> .env```    

## Run
 ```docker run --rm -t --env-file .env now-playing```
